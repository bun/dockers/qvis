#!/bin/sh

# typical way to call this: ./run_server.sh

docker stop qvisserver && docker rm qvisserver

docker run --name qvisserver -p 8080:80 --restart unless-stopped -d qvis/server:latest "$@"

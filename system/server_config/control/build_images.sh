#!/bin/bash

if [ "$#" -ne 1 ]; then
    echo -e "Usage: build_images.sh TAG Ex. build_images.sh oct24"
    echo -e "That will lead to building  qvis/wireshark:oct24 and qvis/server:oct24"
    exit 1
fi

TAG=$1

# wireshark base-image
cd ../../docker_setup/
docker build --no-cache -t qvis/wireshark:$TAG wireshark/
docker tag qvis/wireshark:$TAG qvis/wireshark:latest

# qvisserver module
docker build --no-cache -t qvis/server:$TAG qvis/
docker tag qvis/server:$TAG qvis/server:latest

import app from "./app";
import http from "http";

const port = 80; // 80

http.createServer(app).listen(port, function () {
  console.log("QVIS Server listening on port " + port);
});

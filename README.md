# QVIS Server for a local installation

This is a fork of https://github.com/quiclog/qvis. A live version of this tool can be used at
https://qvis.quictools.info/#/files

## Repository info

- The `src` directory contains the source code and was not modified.
- The `system` directory contains the scripts to create the container and run them

### Local Usage

#### Docker image creation

To create the two container images (wireshark and qvis server) locally, you can run the following script:

```bash
cd system/server_config/control
./build_images.sh <tag>
```

Where tag will be associated with the created images.

For more details on the image creation, take a look to the `dockerfile` in the `docker_setup` directory.

#### Running the QVIS Server

Just run the following script:

```bash
cd system/server_config/control
./run_server.sh
```

And then connect on the local port 8080

## How to use this image

- To download and use this image, just type:
  ```bash
  docker pull registry.forge.hefr.ch/bun/dockers/qvis/qvis:latest
  ```
- To run it locally:
  ```bash
  docker run --privileged --name qvisserver -p 8080:80 --restart unless-stopped -d qvis/server:latest "$@"
  ```

**Remark**: The wireshark image was the based image to create the qvis server image. So it is not used as is.

<hr>
(c) 03.10.2024/BUN
